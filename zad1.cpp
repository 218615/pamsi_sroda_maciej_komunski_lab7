#include <stdlib.h>
#include <stdio.h>
#include <iostream>
#include <time.h>

using namespace std;
class Tablica
{
protected:
    int N;

public:
  virtual void wstaw(int elem) =0;// musi byc =0 bo inaczej wywala blad nie zdefiniowanej refernacji
    virtual bool szukaj(int elem) =0;
    virtual void usun(int elem) =0;
    bool czy_pierwsza(int liczba);
};

bool Tablica::czy_pierwsza(int liczba)
{
    if (liczba < 2) return false;
    for (int i = 2; i <= liczba / 2; ++i)
        if (liczba % i == 0)
            return false;
    return true;
}

struct Elem
{
    Elem* next;
    int wartosc;
};

class Hash : public Tablica
{
private:
    Elem** list;
public:
    Hash(int liczba);

    void wstaw(int elem);
    bool szukaj(int elem);
    void usun(int elem);
};

Hash::Hash(int liczba)
{
    N = liczba;
    list = new Elem*[N];
    for (int i = 0; i < N; i++)
	list[i] = NULL;
}

void Hash::wstaw(int elem)
{
    int index = elem % N;

    Elem* neww = new Elem;
    neww->next = list[index];
    neww->wartosc = elem;
    list[index] = neww;
}

bool Hash::szukaj(int elem)
{
    int i = 1;
    int index = elem % N;

    Elem* temp = list[index];

    while (temp != NULL)
    {
        if (temp->wartosc == elem)
        {
            cout<<"Wykonano :"<< i <<" operacji"<<endl;
            return true;
        }

        temp = temp->next;
        i++;
    }

    cout<<"Wykonano :"<< i <<" operacji"<<endl;
    return false;
}

void Hash::usun(int elem)
{
    int i = 1;
    int index = elem % N;

    Elem** ln = &list[index];
    Elem*  temp = list[index];

    while (temp != NULL)
    {
        if (temp->wartosc == elem)
        {
            Elem* del = temp;
            temp = temp->next;
            delete del;
        }
        else
        {
            *ln = temp;
            ln = &((*ln)->next);
            temp = temp->next;
        }
        i++;
    }

    *ln = NULL;
    cout<<"Wykonano :"<<" " << i <<" operacji"<<endl;
}


class Liniowo : public Tablica
{
private:
    int *tbl;
    int *isempty;
    int size;
public:
    Liniowo(int liczba);

    void wstaw(int elem);
    bool szukaj(int elem);
    void usun(int elem);

};


Liniowo::Liniowo(int liczba)
{
    N = liczba;
    tbl = new int[N];
    isempty = new int[N];
    size = 0;
    for (int i = 0; i < N; i++)
	isempty[i] = 0;
}

void Liniowo::wstaw(int elem)
{
    int i=0;
    int index = elem % N;

    if (size == N) return;


    for (i = 0; i < N; i++)
    {
        if (isempty[(index + i) % N] != 1)
        {
            tbl[(index + i) % N] = elem;
            isempty[(index + i) % N] = 1;
            break;
        }

    }
    cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
    size++;
}

bool Liniowo::szukaj(int elem)
{
    int i;
    int index = elem % N;

    for (i = 0; i < N; i++)
    {
        if (isempty[(index + i) % N] == 0)
        {
            cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
            return false;
        }
        else if (isempty[(index + i) % N] == 1 && tbl[(index + i) % N] == elem)
        {
            cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
            return true;
        }
    }

    cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
    return false;
}

void Liniowo::usun(int elem)
{
    int i;
    int index = elem % N;

    for (i = 0; i < N; i++)
    {
        if (isempty[(index + i) % N] == 0)
        {
            cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
            return;
        }
        else if (isempty[(index + i) % N] == 1 && tbl[(index + i) % N] == elem)
        {
            size--;
            isempty[(index+i)%N] = 2;
        }
    }

    cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
}



class Podwojne : public Tablica
{
private:
    int *tbl;
    int q;
    int *isempty;
    int size;
public:
    Podwojne(int liczba);

    void wstaw(int elem);
    bool szukaj(int elem);
    void usun(int elem);
};

Podwojne::Podwojne(int liczba)
{
    N = liczba;
    tbl = new int[N];
    isempty = new int[N];
    size = 0;
    for (int i = 0; i < N; i++)
        isempty[i] = 0;

    q = 7;
}

void Podwojne::wstaw(int elem)
{
    int i;
    int index = elem % N;

    if (size == N) return;

    for (i = 0; i < N; i++)
    {
        int i = (index + i * (q - (elem % q))) % N;
        if (isempty[i] != 1)
        {
            tbl[i] = elem;
            isempty[i] = 1;
            break;
        }
    }

    cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
    size++;
}

bool Podwojne::szukaj(int elem)
{
    int i;
    int index = elem % N;

    for (i = 0; i < N; i++)
    {
        int i = (index + i * (q - (elem % q))) % N;

        if (isempty[i] == 0)
        {
            cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
            return false;
        }
        else if (isempty[i] == 1 && tbl[i] == elem)
        {
            cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
            return true;
        }
    }

    cout<<"Wykonano :"<< i+1 <<" operacji"<<endl;
    return false;
}

void Podwojne::usun(int elem)
{
    int i;
    int index = elem % N;

    for (i = 0; i < N; i++)
    {
        int i = (index + i * (q - (elem % q))) % N;

        if (isempty[i] == 0)
        {
	   cout<<"Wykonano :"<< i+1 <<"operacji"<<endl;
            return;
        }
        else if (isempty[i] == 1 && tbl[i] == elem)
        {
            size--;
            isempty[i] = 2;
        }
    }

    cout<<"Wykonano :"<< i+1 <<"operacji"<<endl;
}

int main(void)
{

    srand(time(NULL));
	
    Tablica* hash;
    int N, opcja;

    cout <<"Podaj rozmiar tablicy haszujacej: "<< endl;
	cin>>N;
	
	if((hash->czy_pierwsza(N))==true){
    hash = new Liniowo(N);    

    while (opcja!=4)
    {
        cout<<"1 - wstaw"<< endl;
        cout<<"2 - szukaj"<< endl;
        cout<<"3 - usun"<< endl;
	cout<<"4 - koniec" << endl;

	cin>>opcja;
	
	switch(opcja){
	  
	case 1:
	  	for (int i = 0; i <N; i++){
		hash->wstaw(rand() % N);
	}
		break;
	case 2:
	  cout<<"Podaj liczbe :"<< endl;
	  cin >> N;
if (hash->szukaj(N)) cout<<"Podana liczba jest w tablicy"<<endl; else cout<<"Podanej liczby nie ma w tablicy"<<endl;
 break;
	case 3:
	  cout<<"Podaj liczbe :"<< endl;
	  cin >> N;
   hash->usun(N);
	  break;
	default:break;
	}
    }
	}
	else{
	  cout << "ZLA LICZBA"<<endl;}
    return 0;
}