#include <iostream>
#include <cstdlib>
using namespace std;
 

struct Elem
{
    int id; //numer wierzcholka
    struct Elem* next;
};
 
 
struct Lista
{
    struct Elem *glowa;
};
 
 
class Graf
{
    private:
        int V; //ilosc wierzcholkow
        struct Lista* lista;
    public:
        Graf(int V)
        {
            this->V = V;
            lista = new Lista [V];
            for (int i = 0; i < V; ++i)
                lista[i].glowa = NULL; //glowy wierzcholkow na null bo i tak nic nie przechowuja na razie
        }

         //tworzenie listy sasiedztwa
		
        Elem* newElem(int id)
        {
            Elem* newNode = new Elem;
            newNode->id = id;
            newNode->next = NULL;
            return newNode;
        }

         // dodawanie krawedzi do grafu

        void dodaj_krawedz(int zrodlo, int id) // zrodlo jakiego wierzcholka krawedz do id
        {
            Elem* newNode = newElem(id);
            newNode->next = lista[zrodlo].glowa;
            lista[zrodlo].glowa = newNode;
			// graf nie skierowany wiec robimy tez poloczenie z id do scr
            newNode = newElem(zrodlo);
            newNode->next = lista[id].glowa;
            lista[id].glowa = newNode;
        }

        void wypisz()
        {
            int v;
            for (v = 0; v < V; ++v)
            {
                Elem* temp = lista[v].glowa;
				cout<<endl;
                cout<<"Lista sasiedztwa dla wierzcholka numer "<<v<<endl;
				
                while (temp)
                {
                    cout<<" "<<temp->id;
                    temp = temp->next;
                }
                cout<<endl;
            }
        }
};
 

int main()
{
   Graf graf(5);
   
    graf.dodaj_krawedz(0,1);
    graf.dodaj_krawedz(0,3);
    graf.dodaj_krawedz(1,4);
    graf.dodaj_krawedz(1,2);
    graf.dodaj_krawedz(2,3);
    graf.dodaj_krawedz(2,4);
    graf.dodaj_krawedz(3,4);
    graf.wypisz();
 
    return 0;
}