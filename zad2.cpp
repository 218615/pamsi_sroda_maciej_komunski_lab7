#include <iostream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

// Obliczy wartosc maksymalna z dwoch intow, zwraca on a gdy a jest prawdą lub b w przeciwnym wypadku
#define max(a, b) (((a) > (b)) ? (a) : (b))

struct Wezel
{
	Wezel* rodzic;
	Wezel* lewy;
	Wezel* prawy;
	int wartosc;
	int waga; // wspó³czynnik równowagi wêz³a (ró¿nica w wysokoœci poddrzew: lewy - prawy)
};

class DrzewoAVL
{
	void wypisz_wezel(Wezel* wezel);

	Wezel* rotacja_LL(Wezel* wezel);
	Wezel* rotacja_RR(Wezel* wezel);
	Wezel* rotacja_LR(Wezel* wezel);
	Wezel* rotacja_RL(Wezel* wezel);

// szuka mniejszego wezla, jego poprzednika
	Wezel* szukaj_mniejszy(Wezel* p);

	Wezel* usun_pomocniczy(Wezel* x);

	// Obliczy wysokosc poddrzewa w podanym wezla
	int wysokosc(Wezel* w);

	Wezel* korzen;
	
public:
	DrzewoAVL();
	~DrzewoAVL();
	void wstaw(int wartosc);
	// Usuwa element o zadanej wartosci z drzewa. Jesli element nie istnieje, nie robi nic.
	void usun(int wartosc);
	void wypisz_calosc();
	// Szuka wezla w drzewie. Zwraca wskaznik do wezla jesli znajdzie lub NULL, jesli wezel o zadanej wartosci nie istnieje.
	Wezel* szukaj_wezel(int wartosc);

};
DrzewoAVL::DrzewoAVL()
{
	korzen = NULL;
}

DrzewoAVL::~DrzewoAVL()
{}

void DrzewoAVL::wstaw(int wartosc)
{
	// tworzony jest nowy wezel, ktory bedzie wstawiony w odpowiednie miejsce
	Wezel* nowyWezel = new Wezel;
	nowyWezel->wartosc = wartosc;
	nowyWezel->waga = 0;

	Wezel* ostatni = korzen;
	Wezel* rodzic = NULL;

	// poczatkowo jego synowie sa nullami
	nowyWezel->lewy = NULL;
	nowyWezel->prawy = NULL;

	// trzeba znalezc miejsce, w ktore mozna wstawic ten wezel
	while (ostatni != NULL)
	{
		if (ostatni->wartosc == nowyWezel->wartosc)
		{
			// Jeœli znaleziono ju¿ element w drzewie to nie rób nic.
			return;
		}

		rodzic = ostatni; // rodzic to pomocniczy wskaznik przydatny nizej
		ostatni = (nowyWezel->wartosc < ostatni->wartosc ? ostatni->lewy : ostatni->prawy);
	}

	nowyWezel->rodzic = rodzic;

	if (nowyWezel->rodzic == NULL) // jezeli nowyWezel jest korzeniem
	{
		korzen = nowyWezel; // wtedy wstawiamy nowyWezel w miejsce korzenia i konczymy funkcje.
		// Nie trzeba przeprowadzac zadnych rotacji, bo drzewo sklada sie tylko z korzenia.
		return;
	}

	// wstawiamy nowyWezel w miejsce odpowiedniego syna pomocniczego wskaznika rodzic
	if (nowyWezel->wartosc < rodzic->wartosc)
	{
		rodzic->lewy = nowyWezel;
	}
	else
	{
		rodzic->prawy = nowyWezel;
	}

	// wstawiono nowyWezel do rodzica, ktory nie byl zrownowazony. Po wstawieniu jest juz zrownowazony, nie ma potrzeby przeprowadzania rotacji.
	if (rodzic->waga != 0)
	{
		rodzic->waga = 0;
		return;
	}

	// wezel rodzic byl zrownowazony, po wstawieniu nie jest, nalezy zaktualizowac jego flagi
	if (rodzic->lewy == nowyWezel)
	{
		rodzic->waga = 1;
	}
	else
	{
		rodzic->waga = -1;
	}

	Wezel* nadrzedny = rodzic->rodzic;

	// nalezy przejsc cale drzewo od rodzica wskaznika rodzic do korzenia i zaktualizowac flagi
	while (nadrzedny != NULL)
	{
		if (nadrzedny->waga != 0)
		{
			break;
		}

		if (nadrzedny->lewy == rodzic)
		{
			nadrzedny->waga = 1;
		}
		else
		{
			nadrzedny->waga = -1;
		}

		rodzic = nadrzedny;
		nadrzedny = nadrzedny->rodzic;
	}

	if (nadrzedny == NULL)
	{
		return;
	}

	// Po wstawieniu nale¿y wykonaæ obroty w zale¿noœci od flag
	if (nadrzedny->waga == 1)
	{
		if (nadrzedny->prawy == rodzic)
		{
			nadrzedny->waga = 0;
			return; // wezel jest zrownowazony, nie trzeba robic zadnych rotacji
		}

		if (rodzic->waga == -1)
		{
			rotacja_LR(nadrzedny);
		}
		else
		{
			rotacja_LL(nadrzedny);
		}

		return;
	}
	else
	{
		if (nadrzedny->lewy == rodzic)
		{
			nadrzedny->waga = 0;
			return;
		}

		if (rodzic->waga == 1)
		{
			rotacja_RL(nadrzedny);
		}
		else
		{
			rotacja_RR(nadrzedny);
		}

		return;
	}
}

void DrzewoAVL::usun(int wartosc)
{
	Wezel* wezel = szukaj_wezel(wartosc); // jesli nie znajdzie elementow, nie zrobi nic
	if (wezel != NULL)
	{
		usun_pomocniczy(wezel);
	}
	else{
	  cout << " Podany wezel nie istnieje !! "<< endl;}
}

void DrzewoAVL::wypisz_wezel(Wezel* wezel)
{
	// funkcja wypisze pod drzewo razem z ojcem

	if (wezel->rodzic)
	{
		printf("            %02d            \n            |\n", wezel->rodzic->wartosc);
	}
	else
	{
		printf("            X            \n            |\n");
	}

	printf("            %02d\n           /  \\\n", wezel->wartosc);

	if (wezel->lewy)
	{
		printf("         %02d", wezel->lewy->wartosc);
	}
	else
	{
		printf("        X");
	}

	if (wezel->prawy)
	{
		printf("    %02d\n", wezel->prawy->wartosc);
	}
	else
	{
		printf("    X\n");
	}

	if (wezel->lewy)
	{
		printf("        /  \\  ");
	}
	else
	{
		printf("              ");
	}

	if (wezel->prawy)
	{
		printf("/  \\\n");
	}
	else
	{
		printf("\n");
	}

	if (wezel->lewy)
	{
		if (wezel->lewy->lewy)
		{
			printf("       %02d ", wezel->lewy->lewy->wartosc);
		}
		else
		{
			printf("       X ");
		}

		if (wezel->lewy->prawy)
		{
			printf("%02d  ", wezel->lewy->prawy->wartosc);
		}
		else
		{
			printf("X  ");
		}
	}
	else
	{
		printf("               ");
	}

	if (wezel->prawy)
	{
		if (wezel->prawy->lewy)
		{
			printf("%02d ", wezel->prawy->lewy->wartosc);
		}
		else
		{
			printf("X ");
		}

		if (wezel->prawy->prawy)
		{
			printf("%02d ", wezel->prawy->prawy->wartosc);
		}
		else
		{
			printf("X ");
		}
	}

	cout << endl;
}

Wezel* DrzewoAVL::rotacja_LL(Wezel* wezel)
{
	cout << "Rotacja LL:" << endl;
	cout << "Wezel przed: " << endl;
	wypisz_wezel(wezel);

	Wezel* lewy = wezel->lewy;
	Wezel* rodzic = wezel->rodzic;

	wezel->lewy = lewy->prawy;

	cout << "Prawy syn lewego syna idzie w miejsce lewego syna: " << endl;
	wypisz_wezel(wezel);

	if (wezel->lewy)
	{
		wezel->lewy->rodzic = wezel;
	}

	lewy->prawy = wezel;
	lewy->rodzic = rodzic;
	wezel->rodzic = lewy;

	cout << "Poprzedni lewy syn wedruje w miejsce rodzica: " << endl;
	wypisz_wezel(wezel);

	if (rodzic)
	{
		if (rodzic->lewy == wezel)
		{
			rodzic->lewy = lewy;
		}
		else
		{
			rodzic->prawy = lewy;
		}
	}
	else
	{
		korzen = lewy;
	}

	if (lewy->waga == 1)
	{
		wezel->waga = lewy->waga = 0;
	}
	else
	{
		wezel->waga = 1;
		lewy->waga = -1;
	}

	cout << "Koncowa postac wezla: " << endl;
	wypisz_wezel(lewy);

	return lewy;
}

Wezel* DrzewoAVL::rotacja_RR(Wezel* wezel)
{
	cout << "Rotacja RR:" << endl;
	cout << "Wezel przed: " << endl;
	wypisz_wezel(wezel);

	Wezel* prawy = wezel->prawy;
	Wezel* rodzic = wezel->rodzic;

	wezel->prawy = prawy->lewy;

	cout << "Lewy syn prawgo syna idzie w miejsce prawego syna: " << endl;
	wypisz_wezel(wezel);

	if (wezel->prawy)
	{
		wezel->prawy->rodzic = wezel;
	}

	prawy->lewy = wezel;
	prawy->rodzic = rodzic;
	wezel->rodzic = prawy;

	cout << "Poprzedni prawy syn staje sie rodzicem: " << endl;
	wypisz_wezel(wezel);

	if (rodzic != NULL)
	{
		if (rodzic->lewy == wezel)
		{
			rodzic->lewy = prawy;
		}
		else
		{
			rodzic->prawy = prawy;
		}
	}
	else
	{
		korzen = prawy;
	}

	// wezel prawy poprzednio byl niezrownowazony, teraz mozna zmienic jego wage
	if (prawy->waga == -1)
	{
		wezel->waga = 0;
		prawy->waga = 0;
	}
	else
	{
		wezel->waga = -1;
		prawy->waga = 1;
	}

	cout << "Koncowa postac wezla: " << endl;
	wypisz_wezel(prawy);

	return prawy;
}

Wezel* DrzewoAVL::rotacja_LR(Wezel* wezel)
{
	cout << "Rotacja LR:" << endl;
	cout << "Wezel przed: " << endl;
	wypisz_wezel(wezel);

	Wezel* lewy = wezel->lewy;
	Wezel* rodzic = wezel->rodzic;
	Wezel* temp = lewy->prawy;

	lewy->prawy = temp->lewy;

	if (lewy->prawy != NULL)
	{
		lewy->prawy->rodzic = lewy;
	}

	wezel->lewy = temp->prawy;

	if (wezel->lewy != NULL)
	{
		wezel->lewy->rodzic = wezel;
	}

	temp->prawy = wezel;
	temp->lewy = lewy;
	wezel->rodzic = lewy->rodzic = temp;
	temp->rodzic = rodzic;

	cout << "W miejsce wezla idzie prawy syn lewego syna, wezel zas w miejsce swojego poprzedniego prawego syna: " << endl;
	wypisz_wezel(temp);

	if (rodzic)
	{
		if (rodzic->lewy == wezel)
		{
			rodzic->lewy = temp;
		}
		else
		{
			rodzic->prawy = temp;
		}
	}
	else
	{
		korzen = temp;
	}

	wezel->waga = temp->waga == 1 ? -1 : 0;
	lewy->waga = temp->waga == -1 ? 1 : 0;
	temp->waga = 0;

	return temp;
}

Wezel* DrzewoAVL::rotacja_RL(Wezel* wezel)
{
	cout << "Rotacja RL:" << endl;
	cout << "Wezel przed: " << endl;
	wypisz_wezel(wezel);

	Wezel* prawy = wezel->prawy;
	Wezel* rodzic = wezel->rodzic;
	Wezel* temp = prawy->lewy;

	prawy->lewy = temp->prawy;
	if (prawy->lewy != NULL)
	{
		prawy->lewy->rodzic = prawy;
	}

	wezel->prawy = temp->lewy;
	if (wezel->prawy != NULL)
	{
		wezel->prawy->rodzic = wezel;
	}

	temp->lewy = wezel;
	temp->prawy = prawy;
	wezel->rodzic = prawy->rodzic = temp;
	temp->rodzic = rodzic;

	if (rodzic != NULL)
	{
		if (rodzic->lewy == wezel)
		{
			rodzic->lewy = temp;
		}
		else
		{
			rodzic->prawy = temp;
		}
	}
	else
	{
		korzen = temp;
	}

	wezel->waga = temp->waga == -1 ? 1 : 0;
	prawy->waga = temp->waga == 1 ? -1 : 0;
	temp->waga = 0;

	cout << "W miejsce wezla idzie lewy syn prawego syna. Wezel zas wedruje w miejsce swojego poprzedniego lewego syna: " << endl;
	wypisz_wezel(temp);

	return temp;
}

Wezel* DrzewoAVL::szukaj_mniejszy(Wezel* wezel)
{
	Wezel* temp;

	if (wezel != NULL)
	{
		if (wezel->lewy != NULL)
		{
			wezel = wezel->lewy;
			while (wezel->prawy != NULL)
			{
				wezel = wezel->prawy;
			}
		}
		else
		{
			do
			{
				temp = wezel;
				wezel = wezel->rodzic;
			}
			while (wezel != NULL && wezel->prawy != temp);
		}
	}

	return wezel;
}

Wezel* DrzewoAVL::usun_pomocniczy(Wezel* wezel)
{
	// Wykonaj standardowe usuwanie elementu jak w drzewie BST

	Wezel* A;
	bool znaleziono = false;

	if (wezel->lewy && wezel->prawy)
	{
		// nalezy znalezc poprzednik wezla i usunac go, a w miejsce usuniecia wstawic nasz wezel
		A = usun_pomocniczy(szukaj_mniejszy(wezel));
		znaleziono = false;
	}
	else // wezel posiada jednego lub zero synow
	{
		if (wezel->lewy != NULL)
		{
			A = wezel->lewy; // trzeba zapisac pomocniczy wskaznik, ktory pozniej wstawi sie w odpowiednie miejsce.
			// wskaznik ten jest potrzebny, poniewaz nalezy zachowac w drzewie synow usuwanego wezla
			wezel->lewy = NULL;
		}
		else
		{
			A = wezel->prawy;
			wezel->prawy = NULL;
		}

		wezel->waga = 0;
		znaleziono = true;
	}

	if (A != NULL) // istnieje jakis syn ktory trzeba dopisac do drzewa
	{
		A->rodzic = wezel->rodzic;
		A->lewy = wezel->lewy;

		if (A->lewy != NULL)
		{
			A->lewy->rodzic = A;
		}

		A->prawy = wezel->prawy;

		if (A->prawy != NULL)
		{
			A->prawy->rodzic = A;
		}

		A->waga = wezel->waga;
	}

	if (wezel->rodzic != NULL)
	{
		if (wezel->rodzic->lewy == wezel)
		{
			wezel->rodzic->lewy = A;
		}
		else
		{
			wezel->rodzic->prawy = A;
		}
	}
	else // wezel jest korzeniem. Mozna znaleziony wczesniej wezel wstawic w miejsce korzenia.
	{
		korzen = A;
	}

	if (znaleziono)
	{
		Wezel* temp = A;
		A = wezel->rodzic;
		while (A != NULL) // trzeba wykonac operacje dla wszystkich wezlow od A do korzenia
		{
			// wezel A byl w stanie rownowagi przed usunieciem wezla.
			if (A->waga == 0)
			{
				if (A->lewy == temp) // wezel temp nalezy do lewego poddrzewa
				{
					A->waga = -1;
				}
				else // wezel temp nalezy do prawego poddrzewa
				{
					A->waga = 1;
				}
				break;
			}
			else
			{
				if ((A->waga == 1 && A->lewy == temp) || (A->waga == -1 && A->prawy == temp))
				{
					A->waga = 0;
					temp = A;
					A = A->rodzic;
				}
				else
				{
					Wezel* temp = NULL;

					if (A->lewy == temp)
					{
						temp = A->prawy;
					}
					else
					{
						temp = A->lewy;
					}

					if (temp->waga == 0)
					{
						if (A->waga == 1)
						{
							rotacja_LL(A);
						}
						else
						{
							rotacja_RR(A);
						}

						break;
					}
					else if (A->waga == temp->waga)
					{
						if (A->waga == 1)
						{
							rotacja_LL(A);
						}
						else
						{
							rotacja_RR(A);
						}

						temp = temp;
						A = temp->rodzic;
					}
					else
					{
						if (A->waga == 1)
						{
							rotacja_LR(A);
						}
						else
						{
							rotacja_RL(A);
						}

						temp = A->rodzic;
						A = temp->rodzic;
					}
				}
			}
		}
	}

	return wezel;
}

Wezel* DrzewoAVL::szukaj_wezel(int klucz)
{
	Wezel* x = korzen;

	// funkcja standardowo przechodzi drzewo uzywajac relacji porownania, jak w zwyklym BST
	while (x != NULL && x->wartosc != klucz)
	{
		x = klucz < x->wartosc ? x->lewy : x->prawy;
	}

	return x;
}

int DrzewoAVL::wysokosc(Wezel* w)
{
	if (w == NULL)
	{
		return -1;
	}
	else
	{
		int a = wysokosc(w->lewy);
		int b = wysokosc(w->prawy);
		int c = max(a, b) + 1;
		return c;
	}
}

void DrzewoAVL::wypisz_calosc()
{
	int wysokosc_lewa = wysokosc(korzen->lewy) + 1;
	int wysokosc_prawa = wysokosc(korzen->prawy) + 1;

	if (korzen)
	{
		cout << "Korzen: " << korzen->wartosc << endl;

		if (korzen->lewy)
		{
			cout << "Lewy: " << korzen->lewy->wartosc << endl;
		}
		else
		{
			cout << "Lewy: NULL" << endl;
		}

		if (korzen->prawy)
		{
			cout << "Prawy: " << korzen->prawy->wartosc << endl;
		}
		else
		{
			cout << "Prawy: NULL" << endl;
		}
	}
	else
	{
		cout << "Korzen: NULL" << endl;
	}

	cout << "Wysokosc lewego poddrzewa: " << wysokosc_lewa << endl;
	cout << "Wysokosc prawego poddrzewa: " << wysokosc_prawa << endl;
	cout << "Roznica wysokosci: " << wysokosc_lewa - wysokosc_prawa << endl;
}




int main()
{
	DrzewoAVL drzewo;
	
	srand(time(NULL));
	

	for (int i = 0; i < 10; i++)
	{
		drzewo.wstaw(rand() % 10);
	}

	drzewo.wypisz_calosc();


	drzewo.usun(5);

	drzewo.wypisz_calosc();



	while (1)
	{
	}

	return 0;
}